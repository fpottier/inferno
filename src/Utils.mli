(******************************************************************************)
(*                                                                            *)
(*                                  Inferno                                   *)
(*                                                                            *)
(*                       François Pottier, Inria Paris                        *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the MIT License, as described in the file LICENSE.               *)
(*                                                                            *)
(******************************************************************************)

(* [postincrement r] is [r++]. It increments [r] and returns its previous
   value. *)
val postincrement: int ref -> int

(**[gensym()] creates a fresh generator of fresh integer identifiers. *)
val gensym : unit -> (unit -> int)
