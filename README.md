# A library for constraint-based Hindley-Milner type inference.

This software is described in the ICFP 2014 paper
[Hindley-Milner elaboration in applicative style](http://gallium.inria.fr/~fpottier/publis/fpottier-elaboration.pdf)
by François Pottier.

See also the ICFP 2014 [slides](http://gallium.inria.fr/~fpottier/slides/fpottier-2014-09-icfp.pdf).

At present, there is no manual, but
the [API of the latest released version](http://cambium.inria.fr/~fpottier/inferno/doc/inferno/Inferno/)
is online.

See [HACKING.md](HACKING.md) for some information on how to develop the Inferno codebase.
